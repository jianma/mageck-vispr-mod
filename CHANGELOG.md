# Change Log

All notable changes to this project will be documented in this file.

## [0.5.3] - 2016-09-08
### Changed
- Prefer mle rule over rra if design matrix is present.


## [0.5.1] - 2016-03-10
### Added
- It is now possible to activate batch effect correction via combat/sva. The mechanism is described in the example config file that is obtained via "mageck-vispr init".

